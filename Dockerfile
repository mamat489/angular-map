FROM node:current-alpine3.11 as BUILDER
RUN mkdir -p /src/app/
WORKDIR /src/app/
COPY . .
RUN npm install
RUN npm run build:prod

FROM nginx:alpine
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY --from=BUILDER --chown=nginx:nginx /src/app/dist/leaflet-example /usr/share/nginx/html