import { Component, OnInit } from '@angular/core';
import { HereService } from "./here.service";
import { EventEmitterService } from './event-emitter.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
  title = 'leaflet-example';

  countries = [
    {id: 1, name: "1km"},
    {id: 10, name: "10km"},
    {id: 100, name: "100km"}
  ];
 selectedValue = null;

  public query: string;
    public position: string;
    public locations: Array<any>;

    public constructor(private here: HereService,private eventEmitterService: EventEmitterService) {
    }

    selectChange( event: any) {
      //In my case $event come with a id value
      console.log(event);
      this.eventEmitterService.changeDistance(event.id);
    }

    public getAddress(query) {
      if(query != "") {
          this.here.getAddress(query).then(result => {
              this.locations = <Array<any>>result;
              let coordinates: number[] = [this.locations[0].Location.DisplayPosition.Latitude,this.locations[0].Location.DisplayPosition.Longitude];
              this.eventEmitterService.onFirstComponentButtonClick(coordinates); }, error => {
              console.error(error);
          });
      }
      if (document.activeElement instanceof HTMLElement) {
        document.activeElement.blur();
      }
    }
    search(text) {
      alert(text);
    }
}
