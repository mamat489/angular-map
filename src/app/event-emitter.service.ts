import { Injectable, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {

  invokeFirstComponentFunction = new EventEmitter();
  distanceEmitter = new EventEmitter();
  subsVar: Subscription;
  rayon = 1;
  array: number[];

  constructor() { }

  onFirstComponentButtonClick(array: number[]) {
    this.array = array;
    this.invokeFirstComponentFunction.emit(array);
  }


  changeDistance(distance: number) {
    this.rayon = distance;
    console.log(this.array);
    if (this.array !== undefined){
      this.invokeFirstComponentFunction.emit(this.array);
      console.log('ok');
    }
  }
}
