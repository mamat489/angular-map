import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';


declare var H: any;

@Injectable({
    providedIn: 'root'
})
export class HereService {

    public platform: any;
    public geocoder: any;

    public constructor() {
        this.platform = new H.service.Platform({
            "apikey": "_Oa05t9NhxHQAVwaYA1IHuhKTcN_12BAFcVNIaAtTO4"
        });
        this.geocoder = this.platform.getGeocodingService();
    }

    public getAddress(query: string) {
      let headers = new HttpHeaders();
      headers = headers.set('Content-Type', 'application/json; charset=utf-8');
      headers = headers.set('Access-Control-Allow-Origin', '*');

      return new Promise((resolve, reject) => {
          this.geocoder.geocode({ searchText: query }, result => {
              if(result.Response.View.length > 0) {
                  if(result.Response.View[0].Result.length > 0) {
                      resolve(result.Response.View[0].Result);
                  } else {
                      reject({ message: "no results found" });
                  }
              } else {
                  reject({ message: "no results found" });
              }
          }, error => {
              reject(error);
          });
      });
  }
}
