import { AfterViewInit, Component } from '@angular/core';
import * as L from 'leaflet';
import { EventEmitterService } from '../event-emitter.service';
import { HostListener } from "@angular/core";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements AfterViewInit {
  // private map;

  screenHeight: number;
  screenWidth: number;

  constructor(private eventEmitterService: EventEmitterService) { }
  private map;
  private markers;
  private myIcon = L.icon({
    iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/images/marker-icon.png',
    iconAnchor: [12,41]
  },);

  ngAfterViewInit(): void {
    if (this.eventEmitterService.subsVar==undefined) {
      this.eventEmitterService.subsVar = this.eventEmitterService.
      invokeFirstComponentFunction.subscribe((name:number[]) => {
        this.setMarker(name, this.eventEmitterService.rayon);
      });
    }
    this.getScreenSize();

    if(this.screenWidth < 575){
      this.map = L.map('map', {
        center: [ 46.227638, 2.213749 ],
        zoom: 5
      });
    }else{
      this.map = L.map('map', {
        center: [ 46.227638, 2.213749 ],
        zoom: 6
      });
    }
    this.markers = L.layerGroup().addTo(this.map);
    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    tiles.addTo(this.map);

    // L.marker([43.304811, 5.449044], {icon: this.myIcon}).addTo(this.markers).openPopup();
    // L.circle([43.304811, 5.449044], 100000).addTo(this.markers);
  }

  public setMarker(latlon, rayon: number){
    this.markers.clearLayers();
    if(this.screenWidth < 575){
      console.log(rayon);
      switch (rayon) {
        case 1:
          this.map.setView(latlon, 14);
          break;
        case 10:
          this.map.setView(latlon, 10);
          break;
        case 100:
          this.map.setView(latlon, 8);
          break;
      
        default:
          this.map.setView(latlon, 1);
          break;
      }
    } else {
      this.map.setView(latlon, 9);
    }
    L.marker(latlon, {icon: this.myIcon}).addTo(this.markers).openPopup();
    L.circle(latlon, rayon*1000).addTo(this.markers);
  }

  @HostListener('window:resize', ['$event'])
    getScreenSize(event?) {
          this.screenHeight = window.innerHeight;
          this.screenWidth = window.innerWidth;
          console.log(this.screenHeight, this.screenWidth);
    }

}
